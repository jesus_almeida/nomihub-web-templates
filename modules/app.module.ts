import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from '../app/app.component';
import { LandingComponent } from '../components/landing.component';
import { AppRoutingModule } from '../modules/app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
	LandingComponent
  ],
  imports: [
    BrowserModule,
		AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
